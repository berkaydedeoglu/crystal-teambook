package com.uludag.berkay.crystalteambook;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    LinearLayout selected_menu_item;
    LinearLayout menuEntry_Notes, menuEntry_Topics, menuEntry_dScrums;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Objects.requireNonNull(getSupportActionBar()).hide();

        menuEntry_Notes = this.findViewById(R.id.notes_menu_item);
        menuEntry_Topics = this.findViewById(R.id.topics_menu_item);
        menuEntry_dScrums = this.findViewById(R.id.menu_item_daily_scrum);
        selected_menu_item = menuEntry_Notes;

        menuEntry_Notes.setOnClickListener(this::changeMenuItem);
        menuEntry_Topics.setOnClickListener(this::changeMenuItem);
        menuEntry_dScrums.setOnClickListener(this::changeMenuItem);

        fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.main_container, new PersonelNotes(), "Topics");
        transaction.addToBackStack(null);
        transaction.commit();

    }

    private boolean changeMenuItem(View v){
        selected_menu_item.setBackgroundColor(ContextCompat.getColor(this, R.color.bottom_menu));
        v.setBackgroundColor(ContextCompat.getColor(this, R.color.selected_bottom_menu));
        selected_menu_item = (LinearLayout) v;

        if (v.equals(this.menuEntry_Notes)){
            Log.d("MenuItemClicked", "changeMenuItem: Notes");
            FragmentTransaction transaction = this.fragmentManager.beginTransaction();
            transaction.replace(R.id.main_container, new PersonelNotes());
            transaction.addToBackStack(null);
            transaction.commit();
            return true;
        }else if (v.equals(this.menuEntry_Topics)){
            Log.d("MenuItemClicked", "changeMenuItem: Topics");
            FragmentTransaction transaction = this.fragmentManager.beginTransaction();
            transaction.replace(R.id.main_container, new Topics());
            transaction.addToBackStack(null);
            transaction.commit();
            return true;
        }else if (v.equals(this.menuEntry_dScrums)){
            Log.d("MenuItemClicked", "changeMenuItem: Daily Scrums");
            FragmentTransaction transaction = this.fragmentManager.beginTransaction();
            transaction.replace(R.id.main_container, new DailyScrum());
            transaction.addToBackStack(null);
            transaction.commit();
            return true;
        }

        return false;
    }

}
