# Crystal Teambook
**Please read our project document if you are interested. Don't forget this is just a homework.**
[Project Document](./media/Crystal_Report.pdf)

 

## What is Crystal Teambook?
First of all, Crystal Teambook is a small homework project for Uludag University Computer Science which designed and coded by: 

- Yağız Türkmen 
- Osman Berk Aşık
- Berkin Erdem
- Berkay Dedeoğlu

 Crystal Teambook is project management and communication system designed for Agile (especially Scrum) teams.

## Features
### Personal Notes
You can store your basic personal notes via this application.

<p align="center"> 
	<img alt="PersonalNotes" src="./media/personal_notes.png" width=256 />
</p>

Then you need them whenever you want. (Internet connection required)

<p align="center"> 
	<img alt="PersonalNotes" src="./media/personal_notes_detail.png" width=256 />
</p>

### Team Board
You can track your team and your project with Crystal Teambook. Also you can set category for your team note such as bug, feature etc. Your team members can discuss about created team notes.

<p align="center"> 
	<img alt="team boards" src="./media/team_notes.png" width=256>
</p>

### Tracking Daily Scrum
Every morning you can create daily scrum content and you can tag them with your team members. Also you can set alarm which will trigger when the content finished.

<p align="center"> 
	<img alt=dailyscrum src="./media/dailyscrum.png", width=256<
</p>

 
## Architechure?

Crystal Teambook is actually an Android application which is always connected our web services.

And our **web services** means 

- AWS Api Gateway
- AWS DynamoDb
- AWS Lambdas 

We stored your plans, daily scrum notes and personal notes in _DynamoDb_ and we show whenever you need them. Also we put them via a Restfull Api (Api Gateway) and Lambdas.

Our native Android application coded with pure Java.

If you want more deteails, please read [the document](./media/Crystal_Report.pdf).

## Extra Screenshots

<p align="center"> 
	<img src="./media/splash.png" width=256>
	<img src="./media/login.png" width=256>
	<img src="./media/topic.png", width=256>
</p>