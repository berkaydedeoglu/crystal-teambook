package com.uludag.berkay.crystalteambook.Models;

public class Todo {

	public String data;
	public String state;
	public boolean notify;
	
	public Todo(String d, String s) {

		data = d;
		state = s;
		notify = false;
	}
	
}
