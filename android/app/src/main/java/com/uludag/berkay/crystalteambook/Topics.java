package com.uludag.berkay.crystalteambook;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uludag.berkay.crystalteambook.Adapters.TopicsAdapter;
import com.uludag.berkay.crystalteambook.Models.MTopic;

import java.util.LinkedList;


public class Topics extends Fragment {

    FloatingActionButton button_create_topic;
    View fragment_view;
    RecyclerView container;


    public Topics() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.fragment_view = inflater.inflate(R.layout.fragment_topics, container, false);
        return this.fragment_view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.button_create_topic = this.fragment_view.findViewById(R.id.new_topic);
        this.container = this.fragment_view.findViewById(R.id.topic_container);
        this.button_create_topic.setOnClickListener((View v)->{
            Intent intent = new Intent(this.getActivity(), CreateTopicActivity.class);
            this.startActivity(intent);
        });

        LinkedList<MTopic> topics = new LinkedList<>();
        topics.add(new MTopic("Topic 1", "Lorem Ipsum Dolor Sit Amet", "bug"));
        topics.add(new MTopic("Topic 2", "Lorem Ipsum Dolor Sit Amet", "feature"));
        topics.add(new MTopic("Topic 3", "Integer sed pulvinar libero", "bug"));
        topics.add(new MTopic("Topic 4", "Praesent finibus tincidunt nisi", "idea"));
        topics.add(new MTopic("Topic 5", "Lorem Ipsum Dolor Sit Amet", "bug"));

        TopicsAdapter adapter = new TopicsAdapter(topics, this.getContext());
        container.setAdapter(adapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        container.setLayoutManager(linearLayoutManager);
    }
}
