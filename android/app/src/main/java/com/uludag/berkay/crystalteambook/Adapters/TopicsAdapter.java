package com.uludag.berkay.crystalteambook.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uludag.berkay.crystalteambook.Models.MTopic;
import com.uludag.berkay.crystalteambook.R;
import com.uludag.berkay.crystalteambook.ReadTopicActivity;

import java.util.LinkedList;

class TopicsHolder extends RecyclerView.ViewHolder{

    TextView title, type, message;
    View itemView;

    public TopicsHolder(@NonNull View itemView) {
        super(itemView);

        title = itemView.findViewById(R.id.topic_title);
        type = itemView.findViewById(R.id.topic_type);
        message = itemView.findViewById(R.id.topic_context);

        this.itemView = itemView;
    }

    void setViewDatas(MTopic data, Context parent){
        title.setText(data.id);
        message.setText(data.data);
        type.setText(data.type);

        if (data.type.equals("bug")){
            type.setBackgroundResource(R.color.bug);
        }else if (data.type.equals("feature")){
            type.setBackgroundResource(R.color.feature);
        }else if (data.type.equals("idea")){
            type.setBackgroundResource(R.color.idea);
        }

        itemView.setOnClickListener((View v) -> {
            Intent intent = new Intent(parent, ReadTopicActivity.class);
            parent.startActivity(intent);
        });


    }
}

public class TopicsAdapter extends RecyclerView.Adapter<TopicsHolder> {
    LayoutInflater layoutInflater;
    LinkedList<MTopic> dataset;
    Context contex;

    public TopicsAdapter(LinkedList<MTopic> dataset, Context context){
        this.dataset = dataset;
        this.layoutInflater = LayoutInflater.from(context);
        this.contex = context;
    }

    @NonNull
    @Override
    public TopicsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = this.layoutInflater.inflate(R.layout.topic_item, viewGroup, false);
        TopicsHolder holder = new TopicsHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull TopicsHolder topicsHolder, int i) {
        topicsHolder.setViewDatas(dataset.get(i), this.contex);
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }
}
